#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" #http://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
java -cp "$DIR/target/fdio-parquet-gen-1.0-SNAPSHOT.jar":"$DIR/target/lib/"'*' ParquetGen $@
