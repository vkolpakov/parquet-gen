import java.io.File

import org.apache.spark.sql.SparkSession

import scala.util.Random

case class Row(lat1: Float, lon1: Float, lat2: Float, lon2: Float)

object ParquetGen {

  case class Config(rows: Int = 0,
                    partitions: Int = 0,
                    outputDir: File = null,
                    parquetBlockSizeInMB: Int = 1000)

  val parser = new scopt.OptionParser[Config]("parquet generator") {
    opt[Int]('n', "rows").required().action((x, c) =>
      c.copy(rows = x)).text("rows - number of rows in millions")
    opt[Int]('p', "partitions").required().action((x, c) =>
      c.copy(partitions = x)).text("partitions - number of partitions")
    opt[File]('o', "output").required().action((x, c) =>
      c.copy(outputDir = x)).text("output - output dir")
    opt[Int]('b', "block_size").action((x, c) =>
      c.copy(parquetBlockSizeInMB = x)).text("block size in MB")
  }

  val rnd = new Random(0)

  def main(args: Array[String]): Unit = {
    val Config(rowsInMillions, partitions, outDir, parquetBlockSizeInMB) = parser.parse(args, Config()).getOrElse(System.exit(-1))
    val spark = SparkSession
      .builder()
      .master("local[1]")
      .getOrCreate()
    spark.sparkContext.hadoopConfiguration.setInt("parquet.block.size", parquetBlockSizeInMB * 1024 * 1024)

    createParquetFiles(
      spark,
      rowsInMillions,
      partitions,
      outDir.getAbsolutePath
    )
  }

  def toRad(deg: Float): Float = {
    deg * Math.PI.toFloat / 180
  }

  def createRows(ids: Seq[Int]): Seq[Row] = {
    ids.view.map {
      _ =>
        val ts1 = rnd.nextFloat()
        val lat1 = rnd.nextFloat() * 89 * 2 - 89
        val lon1 = rnd.nextFloat() * 179 * 2 - 179
        val ts2 = ts1 + 100 * rnd.nextFloat()
        val lat2 = lat1 + rnd.nextFloat()
        val lon2 = lon1 + rnd.nextFloat()
        Row(lat1 = toRad(lat1), lon1 = toRad(lon1), /*ts2 = ts2, */ lat2 = toRad(lat2), lon2 = toRad(lon2))
    }
  }

  private def createParquetFiles(spark: SparkSession, nRowsInMillions: Int, partitions: Int, parquetPath: String) = {
    import spark.implicits._
    val batchSize = 10000
    val nBatches = nRowsInMillions * 1000 * 1000 / batchSize
    val dataset = (0 until nBatches).toDS().flatMap {
      i =>
        val firstId = i * batchSize
        createRows(firstId until (firstId + batchSize))
    }
    val extraOptions = Map[String, String](
      "parquet.enable.dictionary" -> "false",
      "compression" -> "gzip")

    dataset.repartition(partitions).write.options(extraOptions).mode("overwrite").parquet(parquetPath)
  }
}
